package org.openshift.mlbparks.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;

@Named
@ApplicationScoped
public class DBConnection {

	private MongoDatabase mongoDB;

	public DBConnection() {
		super();
	}

	@PostConstruct
	public void afterCreate() {
		String mongoHost = (System.getenv("MONGODB_SERVICE_HOST") == null) ? "127.0.0.1" : System.getenv("MONGODB_SERVICE_HOST");
		String mongoPort = (System.getenv("MONGODB_SERVICE_PORT") == null) ? "27017" : System.getenv("MONGODB_SERVICE_PORT"); 
		String mongoUser = (System.getenv("MONGODB_USER")== null) ? "mongodb" : System.getenv("MONGODB_USER");
		String mongoPassword = (System.getenv("MONGODB_PASSWORD") == null) ? "mongodb" : System.getenv("MONGODB_PASSWORD");
		String mongoDBName = (System.getenv("MONGODB_DATABASE") == null) ? "mongodb" : System.getenv("MONGODB_DATABASE");
		// Check if we are using a mongoDB template or mongodb RHEL 7 image
		if (mongoHost == null) {
			mongoHost = System.getenv("MONGODB_24_RHEL7_SERVICE_HOST");
		} 
		if (mongoPort == null) {
			mongoPort = System.getenv("MONGODB_24_RHEL7_SERVICE_PORT");
		}
		
		int port = Integer.decode(mongoPort);
		
		//mongoDB = mongo.getDB(mongoDBName);
		
		try {
		MongoCredential credential = MongoCredential.createCredential(mongoUser, mongoDBName, mongoPassword.toCharArray());
		MongoClient mongoClient = new MongoClient(new ServerAddress(mongoHost, Integer.parseInt(mongoPort)), Arrays.asList(credential));
		mongoDB = mongoClient.getDatabase(mongoDBName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*if (mongoDB.authenticate(mongoUser, mongoPassword.toCharArray()) == false) {
			System.out.println("Failed to authenticate DB ");
			System.out.println("Using username: " + mongoUser + " and password: " + mongoPassword.toCharArray());
			System.out.println("PW1 " + mongoPassword + " PW2: " + mongoPassword.toCharArray());
		}
		*/
		
		this.initDatabase(mongoDB);
		
	}

	public MongoDatabase getDB() {
		return mongoDB;
	}

	private void initDatabase(MongoDatabase mongoDB) {
		MongoCollection parkListCollection = mongoDB.getCollection("teams");
		int teamsImported = 0;
		if (parkListCollection.count() < 1) {
			System.out.println("The database is empty.  We need to populate it");
			try {
				String currentLine = new String();
				URL jsonFile = new URL(
						"https://gitlab.com/gshipley/voa-demo/raw/master/src/main/resources/voa.json");
				BufferedReader in = new BufferedReader(new InputStreamReader(jsonFile.openStream()));
				while ((currentLine = in.readLine()) != null) {
					parkListCollection.insertOne(Document.parse(currentLine.toString()));
					teamsImported++;
				}
				System.out.println("Successfully imported: " + teamsImported + " locations.");

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void checkDatabase() {
	    this.initDatabase(mongoDB);
    }

}
